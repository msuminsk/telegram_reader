# Telegram Reader

Using telegram_reader.py you can add a panel widget that displays the current supercycle & user for a specific accelerator.
It has been tested with CentOS 7 (CO virtual machine). The supported machines are: PSB, CPS, SPS.

![Widget](widget.png)

## How it works?
The script subscribes to an accelerator specific XTIM.[accelerator].SCY-CT device, which provides telegram data. Each time there is an update, the script processes USER field and stores current cycle layout to /tmp/telegram.txt. The temporary file is then displayed by a generic widget called [xfce4-genmon-plugin](https://docs.xfce.org/panel-plugins/xfce4-genmon-plugin).

## Installation
* Clone the repository:
`git clone https://gitlab.cern.ch/msuminsk/telegram_reader.git`

Then there are two ways to proceed: installer or manual.

### Installer
Run installer.py script with machine name as the parameter (CPS/PSB/SPS). Note that there are several steps that needs to be done after the installation (see [here](#xfce4-genmon-plugin-configuration)).

### Manual
* Install xfce4-genmon-plugin:
`sudo yum install xfce4-genmon-plugin`
* Add telegram_reader.py to autostart (or keep starting it manually): open menu Applications -> Settings -> Session and Startup -> Application Autostart -> Add and set:
name: Telegram Reader
command: [path/to/telegram_reader]/telegram_reader.py [accelerator]
where [accelerator] is PSB, CPS or SPS
* Add xfce4-genmon-plugin to a panel, either run the command below or right click on the panel, menu Panel -> Add New Items... -> Generic Monitor
`xfce4-panel --add=genmon`
* Configure xfce4-genmon-plugin (described in next [section](#xfce4-genmon-plugin-configuration))

### xfce4-genmon-plugin configuration
This step needs to be done regardless of the installation method. Right click on just added panel widget "(genmon)XXX", select Properties and set:
* Command: cat /tmp/telegram.txt
* Uncheck Label checkbox
* Period (s): 1
