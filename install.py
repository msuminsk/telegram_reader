#!/usr/bin/python3
import sys
import os
import subprocess

def usage():
    print('usage: ./install.py <accelerator>')
    print('<accelerator> is PSB, CPS or SPS')
    exit(1)


if len(sys.argv) != 2:
    usage()

accelerator = sys.argv[1]

if not accelerator in ('PSB', 'CPS', 'SPS'):
    usage()

#######

print('* Starting telegram_reader.py in the background...')
subprocess.Popen("./telegram_reader.py {0} >/dev/null 2>&1 &".format(accelerator), shell=True)

print('* Adding telegram_reader.py to autostart...')
with open(os.path.expanduser('~/.config/autostart/telegram_reader.desktop'), 'w') as f:
    f.write('[Desktop Entry]\n')
    f.write('Encoding=UTF-8\n')
    f.write('Version=0.9.4\n')
    f.write('Type=Application\n')
    f.write('Name=Telegram Reader\n')
    f.write('Comment=\n')
    f.write('Exec={0}/telegram_reader.py {1}\n'.format(os.getcwd(), accelerator))
    f.write('OnlyShowIn=XFCE;\n')
    f.write('StartupNotify=false\n')
    f.write('Terminal=false\n')
    f.write('Hidden=false\n')

print('* Installing xfce4-genmon-plugin (requires password)...')
subprocess.run('sudo yum install -q -y xfce4-genmon-plugin'.split(' '))

print('* Adding xfce4-genmon-plugin to a panel...')
subprocess.run('xfce4-panel --add=genmon'.split(' '))

print('')
print('')
print('The last steps are done manually:')
print('- Right-click on the just added xfce4-genmon-plugin ("genmon(XXX)" label on a panel)')
print('- Select Properties')
print('- In Configuration dialog:')
print('  * Set "Command" to "cat /tmp/telegram.txt"')
print('  * Uncheck "Label" checkbox')
print('  * Set "Period (s)" to 1')
