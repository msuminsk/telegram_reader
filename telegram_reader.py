#!/acc/local/share/python/acc-py/pro/bin/python
import pyjapc
import time
import os
import sys

device = None
selector = None

def usage():
    print('{0} [PSB/CPS/SPS]'.format(sys.argv[0]))

if len(sys.argv) != 2:
    usage()
    exit(1)

if sys.argv[1] == 'PSB':
    device = 'XTIM.BX.SCY-CT'
    selector = 'PSB.USER.ALL'

elif sys.argv[1] == 'CPS':
    device = 'XTIM.PX.SCY-CT'
    selector = 'CPS.USER.ALL'

elif sys.argv[1] == 'SPS':
    device = 'XTIM.SX.SCY-CT'
    selector = 'SPS.USER.ALL'

else:
    usage()
    exit(1)

################################3

super_cycle = []
last_cycle_nb = 0

# generates a text line with pango-compatible text attributes
def text_attribute(text, fg_color=None, bg_color=None, weight=None):
    options = ''

    if fg_color:
        options += ' foreground="{0}"'.format(fg_color)

    if bg_color:
        options += ' background="{0}"'.format(bg_color)

    if weight:
        options += ' weight="{0}"'.format(weight)

    return '<span{0}>{1}</span>'.format(options, text)


# XTIM.xx.SCY-CT subscription handler
def subscription_cb(parameter_name, data, header):
    global super_cycle
    global last_cycle_nb

    user = data['USER']
    cycle_nb = data['CYCLE_NB']

    # shorten the supercycle if it has just changed
    if cycle_nb == 1 and last_cycle_nb != 0:
        super_cycle = super_cycle[:last_cycle_nb]

    last_cycle_nb = cycle_nb

    # resize the super_cycle array to put the current USER in the right spot
    while len(super_cycle) < cycle_nb:
        super_cycle.append('')

    super_cycle[cycle_nb - 1] = user

    # generate file for genmon widget
    with open("/tmp/telegram.txt", "w") as output:
        output.write('<txt>')

        for i in range(len(super_cycle)):
            if i == cycle_nb - 1:
                output.write(text_attribute(super_cycle[i] + ' ', weight="bold"))
            else:
                output.write(text_attribute(super_cycle[i] + ' ', fg_color="gray"))

        output.write('</txt>')

    # TODO it does not work
    #os.system('xfce4-panel --plugin-event=genmon-8:refresh:bool:true')


# main loop
japc = pyjapc.PyJapc(incaAcceleratorName=None)

try:
    japc.subscribeParam(parameterName='{0}/Acquisition'.format(device),
        onValueReceived=subscription_cb, timingSelectorOverride=selector,
        getHeader=True)
    japc.startSubscriptions()

    while True:
        time.sleep(0.2)

except KeyboardInterrupt as e:
    japc.stopSubscriptions()
    japc.clearSubscriptions()

except Exception as e:
    print(e)
    japc.stopSubscriptions()
    japc.clearSubscriptions()
